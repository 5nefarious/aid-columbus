# AID Columbus

This repository contains the AID Columbus website.

## Building

### Required Software

- GNU Make
- ImageMagik

### Optional Software

- darkhttpd

The provided server script is intended to work with darkhttpd, although any
other webserver can be substituted.

### Generating Assets

1. Go into the `assets` directory
1. Run `make`

This should generate PNG images using the vector files included in the repo.

### Testing the Site

1. Create a root directory for the server, e.g. - `~/.public_html/`
1. Make sure the directory has executable permissions set for everyone. Run these commands:

	chmod o+x ~/.public_html
	chmod -R o+r ~/.public_html

1. Ensure that your home directory is also accessible:
	
	chmod o+x ~

1. Change to the root directory of the repository.
1. Generate assets if necessary.
1. Run `make` to copy the contents of the repository into `~/.public_html`
1. Run `./server.sh` to start darkhttpd on port 8080.

The website should now be reachable at http://localhost:8080/

